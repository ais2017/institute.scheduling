Institute.Scheduling
####################

Документация
------------

Для сборки документации в формате HTML предлагается воспользоваться следующей
последовательностью команд:

.. code:: bash

   pushd docs
   make html
   popd > /dev/null

Собранная документация будет доступна в :code:`docs/_build/index.html`.
