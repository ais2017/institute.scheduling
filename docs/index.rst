.. Institute.Scheduling documentation master file, created by
   sphinx-quickstart on Sun Sep 23 17:22:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Institute.Scheduling's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   specification/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
